import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcaptureNgUiComponent } from './ecapture-ng-ui.component';

describe('EcaptureNgUiComponent', () => {
  let component: EcaptureNgUiComponent;
  let fixture: ComponentFixture<EcaptureNgUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcaptureNgUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcaptureNgUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
