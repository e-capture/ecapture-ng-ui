import { NgModule } from '@angular/core';
import { EcaptureNgUiComponent } from './ecapture-ng-ui.component';
import { ButtonComponent } from './button/button.component';
import {CommonModule} from "@angular/common";



@NgModule({
  declarations: [
    EcaptureNgUiComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    EcaptureNgUiComponent,
    ButtonComponent
  ]
})
export class EcaptureNgUiModule { }
