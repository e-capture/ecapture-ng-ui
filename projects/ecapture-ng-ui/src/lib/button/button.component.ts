import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ec-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {


  @Input() type = "";
  @Input() label = '';
  @Input() icon = '';
  @Input() bgColor = '';
  @Input() txtColor = '';
  @Input() brColor = '';
  @Input() ecStyles = '';
  @Input() ecClass = '';

  public style = '';

  public backgroundColor = "";
  public textColorBase = "";
  public borderColorBase = "";

  constructor() {
  }

  public ngOnInit(): void {
    this.setButtonColor();
  }

  private setButtonColor(): void {
    if (this.bgColor === '' && this.txtColor === '' && this.brColor === '') {
      this.backgroundColor = 'bg-container-orange-3';
      this.textColorBase = 'text-white';
      this.borderColorBase = 'border-transparent';
    } else if (this.bgColor === '' && this.brColor === '') {
      this.backgroundColor = 'bg-transparent';
      this.textColorBase = this.txtColor;
      this.borderColorBase = 'border-transparent';
    } else if (this.bgColor === '') {
      this.backgroundColor = 'bg-transparent';
      this.textColorBase = this.txtColor;
      this.borderColorBase = this.brColor;
    } else if (this.brColor === '') {
      this.backgroundColor = this.bgColor;
      this.textColorBase = this.txtColor;
      this.borderColorBase = 'border-transparent';
    } else {
      this.backgroundColor = this.bgColor;
      this.textColorBase = this.txtColor;
      this.borderColorBase = this.brColor;
    }
  }

}
